(function() {
  
  // print("Mmmm... chocolate!"); // phase 0 -- testing and setup
  
  function print(val){
    // rxjs helper function to print results in web page
    // source at angularfirebase.com

    let el = document.createElement('p');
    el.className = 'lead';
    el.innerText = val;
    el.style.color = "#0069FF";
    document.querySelector('#observationpoint').appendChild(el);
    let hr = document.createElement( 'hr' );
    document.querySelector('#observationpoint').appendChild(hr);
  }
  // ######################################################## 
  // lesson 03 ~ .map() Operator -- @5m41s -- .map allows you 
  // to transform the transmitted value based on some underlying logic -- use 'return' KEYWORD
  // see https://www.reddit.com/r/Angular2/comments/5u5orx/response_coming_as_undefined_inside_subscribe/

  const numbers = Rx.Observable.of( 10, 100, 1000 );
  numbers
  .map(
    // (num) => { Math.log(num); } // returns undefined across the board... 
    (num) => { return Math.log(num); }
  )
  .subscribe(
    (x) => { print(x); }
  );

  const jsonString = '{ "type": "Dog", "breed": "Pug" }';
  const apiCall = Rx.Observable.of( jsonString );

  apiCall.map(
    (json) => {return JSON.parse(json);}
  )
  .subscribe(
    (obj) => { print(obj.type); print(obj.breed); }
  );

  // ######################################################## 
  // lesson 04 ~ .do() Operator -- @6m20s -- this allows me to execute code
  // without affecting the underlying observable

  const names = Rx.Observable.of( 'Simon', 'Garfunkle' );
  names
  .do(
    (name) => { print(name); }
  )
  .map(
    (name) => { return name.toUpperCase(); }
  )
  .do(
    (name) => { print(name); }
  )
  .subscribe(
    () => {}
  );

  // ######################################################## 
  // lesson 05 ~ .filter() Operator -- @6m48s -- a condition is applied, and only values
  // meeting that criteria make it through...
  const integers = Rx.Observable.of(-3,5,7,2,-7,9,-2);
  integers
  .filter(
    // (num) => { return num >= 0; } // show positive numbers
    (num) => {return num < 0; } // show negative numbers
  )
  .subscribe(
    (x) => { print(x); }
  );

  // ######################################################## 
  // lesson 06 ~ .first() and .last() Operators -- @7m14s -- returns the first and last elements respectively
  // ELICITS TypeError: samenumbers.first is not a function, hence, utilize rx.aggregates library

  const samenumbers = Rx.Observable.of(-210,45,-3,5,7,2,-7,9,-2,100,-77);
  samenumbers
  .last()
  .subscribe(
    (x) => { print(x); }
  );

  samenumbers
  .first()
  .subscribe(
    (x) => { print(x); }
  );
  
})();