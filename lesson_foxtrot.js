(function() {
  
  // print("Mmmm... chocolate!"); // phase 0 -- testing and setup
  
  function print(val){
    // rxjs helper function to print results in web page
    // source at angularfirebase.com

    let el = document.createElement('p');
    el.className = 'lead';
    el.innerText = val;
    el.style.color = "#5656EE";
    document.querySelector('#observationpoint').appendChild(el);
    let hr = document.createElement( 'hr' );
    document.querySelector('#observationpoint').appendChild(hr);
  }
  
  // ######################################################## 
  // lesson 09 ~ .switchMap() Operator -- @9m30s -- switchMap() is useful for getting a SECOND value
  // from another Observable AFTER the FIRST value has been obtained from the INITIAL Observable in this case we
  // are not concerned with the click events than we are with (re)establishing an INTERVAL timer with each CLICK

  // Timing function Operators CAN include .interval() .timer() .finally() .unsubscribe()
  // .unsubscribe()\.dispose() manually forces the Observable to send the complete signal
  // ELICITS TypeError subscription.unsubscribe is not a function -- use .dispose()

  // reestablish the count to ZERO with each click... use case could be having an INITIAL Observable of UserID, then,
  // querying a DB with that UserID for that user's data

  /*let clicks = Rx.Observable.fromEvent( document, 'click' );
  clicks
  .switchMap(
    (click) => { return Rx.Observable.interval(703); }
  )
  .subscribe(
    (i) => {print(i);}
  ); */

  // ######################################################## 
  // lesson 10 ~ .takeUntil() Operator -- @10m11s -- this allows us to complete an Observable based on the value
  // of another Observable -- this constitutes another way to dispose()\unsubscribe() without explicitly calling it.

  const interval = Rx.Observable.interval(703);
  const notifier = Rx.Observable.timer(2109);

  interval
  .takeUntil( notifier )
  .finally(
    () => {print('Complete, baby!');}
  )
  .subscribe(
    (i) => {print(i);}
  ); 

  // ######################################################## 
  // lesson 11 ~ .takeWhile() Operator -- @10m40s -- tells Observable to emit values until a certain condition is true
  const mugiwara = Rx.Observable.of('Luffy','Usopp','Franky','Caribou','Robin');

  mugiwara
  .takeWhile(
    name => name !== 'Caribou' // this is okay
    // (name) => { name !== 'Caribou'; } // borks it!
  )
  .finally(
    () => {print( 'Complete! I found Caribou.' ); }
  )
  .subscribe(
    (i) => {print(i);}
  );
  
})();