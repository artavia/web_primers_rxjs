(function() {
  
  // print("Mmmm... chocolate!"); // phase 0 -- testing and setup
  
  function print(val){
    // rxjs helper function to print results in web page
    // source at angularfirebase.com

    let el = document.createElement('p');
    el.className = 'lead';
    el.innerText = val;
    el.style.color = "#BADA55"
    document.querySelector('#observationpoint').appendChild(el);
    let hr = document.createElement( 'hr' );
    document.querySelector('#observationpoint').appendChild(hr);
  }
  
  // lesson 02 ~ Observable Completion -- @4m52s
     // Operators include .interval() .timer() .finally() .unsubscribe()
  // When Observables reach the end of their lifecycle, they leave a completed a signal. 
  // .finally() operator makes this evident
  // .unsubscribe()\.dispose() manually forces the Observable to send the complete signal
  // ELICITS TypeError subscription.unsubscribe is not a function
  // hence, import rx.testing in order to cure defect WILL NOT FIX, and use .dispose()
  // instead and refer to:  https://stackoverflow.com/questions/43970970/rxjs-subscription-unsubscribe-is-not-a-function 

  const leakyinterval = Rx.Observable.interval(500).finally( () => {print( 'All done!' );} );

  const subscription = leakyinterval.subscribe(
    (x) => {print(x);}
  );
  
  setTimeout(
    // () => {subscription.unsubscribe();}
    () => {subscription.dispose();}
  , 3000 );
})();