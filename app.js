(function() {
  
  print("eat more fish"); // phase 0 -- testing and setup
  
  function print(val){
    // rxjs helper function to print results in web page
    // source at angularfirebase.com

    let el = document.createElement('p');
    el.className = 'lead';
    el.innerText = val;
    document.querySelector('#observationpoint').appendChild(el);
  }
  
  // lesson 01 ~ RxJS Observable -- @41s
  
  // lesson 1.5 ~ Hot vs. Cold Observables -- @3m27s

  // lesson 02 ~ Observable Completion -- @4m52s
      // Operators include .timer() .finally() .unsubscribe()

  // lesson 03 ~ .map() Operator -- @5m41s
  // lesson 04 ~ .do() Operator -- @6m20s
  // lesson 05 ~ .filter() Operator -- @6m48s
  // lesson 06 ~ .first() and .last() Operators -- @7m14s
  // lesson 07 ~ .debounceTime() and .throttleTime() Operators -- @7m38s
  // lesson 08 ~ .scan() Operator -- @8m37s
  // lesson 09 ~ .switchMap() Operator -- @9m30s
  // lesson 10 ~ .takeUntil() Operator -- @10m11s
  // lesson 11 ~ .takeWhile() and .finally() Operators -- @10m40s
  // lesson 12 ~ .zip() Operator -- @11m9s
  // lesson 13 ~ .forkJoin() Operator -- @11m34s
  // lesson 14 ~ .catch() Operator -- @12m5s
  // lesson 15 ~ .retry() Operator -- @12m38s

  // lesson 16 ~ RxJS Subject-- @13m09s

  // lesson 17 ~ .multicast() @14m02s to the end

})();