(function() {
  
  // print("Mmmm... chocolate!"); // phase 0 -- testing and setup
  
  function print(val){
    // rxjs helper function to print results in web page
    // source at angularfirebase.com

    let el = document.createElement('p');
    el.className = 'lead';
    el.innerText = val;
    el.style.color = "#FF0069";
    document.querySelector('#observationpoint').appendChild(el);
    let hr = document.createElement( 'hr' );
    document.querySelector('#observationpoint').appendChild(hr);
  }
  
  // lesson 1.5 ~ Hot vs. Cold Observables -- @3m27s

  // Cold Observable is an Observable where data is constructed inside of it.
  // Meaning that it will not create the underlying data until something subscribes to it.
  // with coldone, the random number gets generated once the subscription starts
  // and each subscriber gets a different number
  const coldone = Rx.Observable.create(
    (observer) => {observer.next( Math.random() ); }
  );
  coldone.subscribe(
    (a) => { print(`c1 Subscriber A: ${a}`); }
  );
  coldone.subscribe(
    (b) => { print(`c1 Subscriber B: ${b}`); }
  ); 

  // Hot Observable means that a value is built outside of the Observable itself.
  const hotmath = Math.random();
  const hottwo = Rx.Observable.create(
    (observer) => {observer.next( hotmath ); }
  );
  hottwo.subscribe(
    (a) => { print(`h2 Subscriber A: ${a}`); }
  );
  hottwo.subscribe(
    (b) => { print(`h2 Subscriber B: ${b}`); }
  ); 

  // second manner to make a hot observable without data decoupling is 
  // to call .publish on the cold observable and corresponding .connect method on the data.
  const coldthree = Rx.Observable.create(
    (observer) => {observer.next( Math.random() ); }
  );
  const hotthree = coldthree.publish();
  hotthree.subscribe(
    (a) => { print(`h3 Subscriber A: ${a}`); }
  );
  hotthree.subscribe(
    (b) => { print(`h3 Subscriber B: ${b}`); }
  ); 
  hotthree.connect();

})();