(function() {
  
  // print("Mmmm... chocolate!"); // phase 0 -- testing and setup
  
  function print(val){
    // rxjs helper function to print results in web page
    // source at angularfirebase.com

    let el = document.createElement('p');
    el.className = 'lead';
    el.innerText = val;
    el.style.color = "#989898"
    document.querySelector('#observationpoint').appendChild(el);
    let hr = document.createElement( 'hr' );
    document.querySelector('#observationpoint').appendChild(hr);
  }

  // ######################################################## 
  
  // lesson 01 ~ RxJS Observable -- @41s
  
  // Operators include .subscribe()
  // create an observable from scratch with .create() method
  // which receives an observer function where we can define
  // what the Observable sends to the subscriber
  // Then, to make the Observable emit values, you call the subscribe() method.

  const observable = Rx.Observable.create( 
    observer => {
      observer.next('hello');
      observer.next('world');
    } 
  );

  observable.subscribe(
    val => { print(val); }
  );

  // create Observables from events in the DOM
    // ELICITS a TypeError: Rx.Observable.fromEvent is not a function
    // to resolve, include rx.async and rx.binding LIBS
  // 
  const clickEvents = Rx.Observable.fromEvent( document, 'click' );
  clickEvents.subscribe(
    click => {console.log(click); print(click.type);}
  );

  // convert a Promise into an Observable
  // simulate an API call or some other asynchronous operation with setTimeout
  // convert an Observable back into a Promise by calling .toPromise()
  const promise = new Promise( ( resolve, reject ) => {
    setTimeout( () => {
      resolve('resolved, baby!');
    } , 703);
  } );
  const obsvPromise = Rx.Observable.fromPromise( promise );
  obsvPromise.subscribe( result => print(result) );

  // create a timer by calling .timer() that accepts a parameter for 
  // amount of milliseconds to delay. To emit, then, call subscribe.
    // ELICITS a TypeError: Rx.Observable.timer is not a function
    // to resolve, include rx.timing

  const timer = Rx.Observable.timer(1406);
  timer.subscribe(
    // done => print('bada-bing!')
    (done) => { print('bada-bing!'); } 
  );

  // If you want a repeating timer like a setInterval use .interval
  const interval = Rx.Observable.interval(2812);
  // interval.subscribe( (int) => { print( new Date().getSeconds() ); }  );

  // .of allows me to pass in any static value to the Observable
  const mashup = Rx.Observable.of(
    'me love you'
    , ['long', 'time' ]
    , 23
    , true
    , {cool: 'stuff'}
  );

  mashup.subscribe( val => {print(val); } );

  // ######################################################## 
  // lesson 02 ~ Observable Completion -- @4m52s
      // Operators include .timer() .finally() .unsubscribe()

  

})();