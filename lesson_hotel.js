(function() {
  
  // print("Mmmm... chocolate!"); // phase 0 -- testing and setup
  
  function print(val){
    // rxjs helper function to print results in web page
    // source at angularfirebase.com

    let el = document.createElement('p');
    el.className = 'lead';
    el.innerText = val;
    el.style.color = "#5E255D";
    document.querySelector('#observationpoint').appendChild(el);
    let hr = document.createElement( 'hr' );
    document.querySelector('#observationpoint').appendChild(hr);
  }
  
  // ######################################################## 
  // lesson 16 ~ RxJS Subject-- @13m09s -- subject is an Observable with a few additional features.
  // and it has the ability to emit new data to Subscribers by acting as a proxy to some other data source.

  // const observable = Rx.Observable.of( 'Hello' );
  // const obA = observable.subscribe( val => print(`Ob A: ${val}`) );
  // const obB = observable.subscribe( val => print(`Ob B: ${val}`) );
  
  // const subject = Rx.Subject.of( 'Hello' ); // ELICITS TypeError: Rx.Subject.of is not a function

  const subject = new Rx.Subject();
  const subA = subject.subscribe( val => print(`Sub A: ${val}`) );
  const subB = subject.subscribe( val => print(`Sub B: ${val}`) );
  // subject.next( 'Hello' ); // ELICITS TypeError: subject.next is not a function ... use onNext() in lieu of next()
  subject.onNext( 'Hello' );
  setTimeout( () => { subject.onNext( 'World' );} );

  // ######################################################## 
  // lesson 17 ~ .multicast() @14m02s to the end -- is used to send values to multiple subscribers but not
  // any related side-effects
  // ELICITS this._fn2 is not a function -- BORKULA!!! - searched "rxjs multicast() + this._fn2 is not a function"
  // took me here:  https://github.com/Reactive-Extensions/RxJS/blob/master/doc/api/core/operators/multicast.md

  /*const observable = Rx.Observable.fromEvent( document, 'click' );
  const clicks = observable.do( (_) => {print('Do One Time!');} );
  
  const anothersubject = clicks.multicast( () => new Rx.Subject() );
  const subC = anothersubject.subscribe( clk => print(`Sub C: ${clk.timeStamp}`) );
  const subD = anothersubject.subscribe( clk => print(`Sub D: ${clk.timeStamp}`) );

  subject.connect();*/
  // ######################################################## 
  
})();