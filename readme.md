# Description
RxJs for web primer&hellip; yeah, it&rsquo;s a thing!

## What it is
It&rsquo;s an appetizer. It&rsquo;s like primer that goes on an automobile before a new paint job. The purpose of this is to introduce you to the subject of RxJs. RxJs is a library that is included in every new Angular-Cli project. Thus, it is advisable that one gets intimately acquainted with RxJs. 

## Preparatory steps
You will need a **text editor** and a deploy&#45;anywhere, front&#45;end oriented webserver such as **[http-server](https://www.npmjs.com/package/http-server "link to npm repository for http-server")** (which I recommend that you install globally) or **Web Server for Chrome**.

## Helpful resources
In [another project](https://github.com/donlucho/googlemaps-geo "link to don Lucho&rsquo;s other repo") that I only recently published, I mentioned a couple of helpful resources to get you acquainted with RxJs. I may or may not (I have not decided yet&hellip;) publish yet another resource to accompany this repository but in the context of [Angular v5](https://v5.angular.io "link to Angular v5"). 

I had sung the praises of a video by the [Angular Firebase team on YouTube](https://www.youtube.com/channel/UCsBjURrPoezykLs9EqgamOA/videos "link to user Angular Firebase at YouTube"). This repository is what resulted from watching that 15 minute video. The video itself which is called [RxJS Quick Start with Practical Examples](https://www.youtube.com/watch?v=2LCo926NFLI "link to RxJS Quick Start with Practical Examples") and it can be of real help to any developer. I recommend it. 

## Purpose of this toy project
To be of service to others. To be the salt of the earth. To lead by example. To fight the good fight and to renounce all evil in all of its forms. Time is short&hellip; and, I just saved you a bunch of it!

## My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!