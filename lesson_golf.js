(function() {
  
  // print("Mmmm... chocolate!"); // phase 0 -- testing and setup
  
  function print(val){
    // rxjs helper function to print results in web page
    // source at angularfirebase.com

    let el = document.createElement('p');
    el.className = 'lead';
    el.innerText = val;
    el.style.color = "#470a0a";
    document.querySelector('#observationpoint').appendChild(el);
    let hr = document.createElement( 'hr' );
    document.querySelector('#observationpoint').appendChild(hr);
  }
  
  // ######################################################## 
  // lesson 12 ~ .zip() Operator -- @11m9s -- for combining observables -- useful if, for example,
  // you have two observables of the same length and they are related in some other way. .zip() will combine
  // them into arrays based on their original index positions

  const nouns = Rx.Observable.of('Usopp','Chopper','Haki');
  const descriptions = Rx.Observable.of('Sniper extraordinaire','first-rate doctor','Spirit repackaged');

  const combo = Rx.Observable.zip( nouns, descriptions );
  combo.subscribe( arr => print(arr) );

  // ######################################################## 
  // lesson 13 ~ .forkJoin() Operator -- @11m34s -- for combining observables -- this will wait for the 
  // both of the observables to complete, then, it will combine the last two values together. Hence, there will be
  // a delay, then, only the last two values will be emitted.
  // Runs two observable sequences in parallel and combines their last elements.
  // ELICITS TypeError: Rx.Observable.forkJoin is not a function... hence, bring in rx.experimental
  // Useful if you have a bunch of related API calls and you want to wait for all of them to resolve before sending any data to the UI

  const characters = Rx.Observable.of('Usopp','Chopper','Monkey D. Garrp');
  const featured = Rx.Observable.of('Sniper extraordinaire','first-rate doctor','Marine Vice Admiral').delay(1406);

  const zipparooni = Rx.Observable.forkJoin( characters, featured );
  zipparooni.subscribe( arr => print(arr) );


  // ######################################################## 
  // lesson 14 ~ .catch() Operator -- @12m5s --  
  // ELICITS TypeError: result is undefined ... 
  // lesson 15 ~ .retry() Operator -- @12m38s -- .retry() will rerun for as many times as indicated before it gives up
  
  /*const derrpobservable = Rx.Observable
  .create(
    (observer) => {
      observer.next('good');
      observer.next('great');
      observer.next('awesome');
      throw 'Hey, bubba! You have an error!';
      // observer.error('Hey, bubba! You have an error!' );
      observer.next('un-flipping-believable');
    }
  );

  derrpobservable
  .catch(
    (err) => { print( `Error caught, doofus: ${err}` ); } // BORKS with aforementioned TypeError
  )
  .retry(2)
  .subscribe(
    (val) => { print(val); }
  );*/
  
})();