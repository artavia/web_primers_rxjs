(function() {
  
  // print("Mmmm... chocolate!"); // phase 0 -- testing and setup
  
  function print(val){
    // rxjs helper function to print results in web page
    // source at angularfirebase.com

    let el = document.createElement('p');
    el.className = 'lead';
    el.innerText = val;
    el.style.color = "#00FF00";
    document.querySelector('#observationpoint').appendChild(el);
    let hr = document.createElement( 'hr' );
    document.querySelector('#observationpoint').appendChild(hr);
  }
  
  // ######################################################## 
  // lesson 07 ~ .debounceTime() and .throttleTime() Operators -- @7m38s
  // throttleTime gets the very first event, then, a delay can be set to get any other events
  // ELICITS TypeError: mouseEvents.throttleTime is not a function, hence, accommodate Observable with throttle keyword
  // debounceTime gets the last event, then, a delay can be set to get any other more recent events-- useful for confirming that the user has stopped doing something or performing actions such as in keyup event\autocomplete form
  // ELICITS TypeError: mouseEvents.debounceTime is not a function, hence, accommodate Observable with debounce keyword
  
  let mouseEvents = Rx.Observable.fromEvent( document, 'mousemove' );
  mouseEvents
  .throttle(5625)
  // .debounce(5625)
  .subscribe(
    (e) => { print(e.type); }
  );
  
  // ######################################################## 
  // lesson 08 ~ .scan() Operator -- @8m37s-- scan keeps a running total of each emitted value from the Observable
  // -- ALSO, use 'return' KEYWORD to resolve

  // .fromEvent() create Observables from events in the DOM
    // ELICITS a TypeError: Rx.Observable.fromEvent is not a function
      // to resolve, include rx.async and rx.binding LIBS

  // .map() Operator -- @5m41s -- .map allows you to transform the transmitted value based on 
  // some underlying logic -- use 'return' KEYWORD to resolve

  // .do() Operator -- @6m20s -- this allows me to execute code without affecting the underlying observable

  let clickEvents = Rx.Observable.fromEvent( document, 'click' );
  clickEvents
  .map(
    (e) => { return parseInt( Math.random() * 10 ); }
  )
  .do(
    (score) => { print( `Click scored + ${score}` ); }
  )
  .scan(
    (highScore, score ) => { return highScore + score;}
  )
  .subscribe(
    (highScore) => { print(`High Score ${highScore}`); }
  );
  
})();